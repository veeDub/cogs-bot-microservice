FROM gcr.io/google-appengine/nodejs

WORKDIR /src

COPY ["package.json", "package-lock.json", "./"]
RUN npm install

COPY out /src/out

EXPOSE 8080
CMD npm start