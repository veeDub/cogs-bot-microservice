# microservice-modules-COGS
This is the backend to support the COGS department advising bot.
Written in typescript with a node server. API docs in Swagger, and project bootstrapped with Grunt.

grunt build
grunt run
