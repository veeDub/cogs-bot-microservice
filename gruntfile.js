/**
 * Created by valeriewyns on 2018-05-06.
 */

module.exports = function (grunt) {

    grunt.initConfig({
        mochaTest: {
            test: {
                options: {
                    reporter: 'spec',
                    captureFile: 'results.txt'
                },
                src: ['out/test/*.js']
            }
        },

        ts: {
            build: {
                src: ["\*\*/\*.ts", "!node_modules/\*\*/\*.ts"],
                outDir: "out",
                options: {
                    module: 'commonjs',
                    fast: 'never',
                    inlineSourceMap: true
                }
            }
        },
        nodemon: {
            dev: {
                script: 'out/src/app.js'
            },
            options: {
                ignore: ['node_modules/**', 'gruntfile.js'],
                env: {PORT: '8080'}
            }
        }
    });
    grunt.loadNpmTasks('grunt-mocha-test');
    grunt.loadNpmTasks("grunt-ts");
    grunt.loadNpmTasks('grunt-nodemon');


    grunt.registerTask("build", ["ts"]);
    grunt.registerTask("run", ["ts", "nodemon"]);


};
