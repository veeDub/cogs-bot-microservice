"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Datastore = require('@google-cloud/datastore');
var projectID = "microservice-modules-cogs";
var datastore = new Datastore({ projectId: projectID });
function getAllFromDataStore() {
    console.log("getting all from the store");
    var query = datastore
        .createQuery('module')
        .order('course_title', {
        descending: true,
    });
    datastore
        .runQuery(query).then(function (results) {
        console.log("thenning: " + results.length);
        var all_modules = results[0];
        console.log('Modules: ');
        all_modules.forEach(function (module) { return console.log(module.course_title); });
    }).catch(function (error) {
        console.log(error);
    });
    return "all was got yo!";
}
exports.getAllFromDataStore = getAllFromDataStore;
function getFromDataStoreByDept(req) {
    console.log("getting some from the store by dept");
    var dept = req.params.course_dept;
    console.log("dept: " + dept);
    var query = datastore
        .createQuery('module')
        .filter('course_dept', '=', "ANTH")
        .order('course_title', {
        descending: true,
    });
    datastore
        .runQuery(query).then(function (results) {
        console.log("thenning: " + results.length);
        var all_modules = results[0];
        console.log('Modules: ');
        all_modules.forEach(function (module) { return console.log(module.course_title); });
    }).catch(function (error) {
        console.log(error);
    });
    return "all was got by dept";
}
exports.getFromDataStoreByDept = getFromDataStoreByDept;
function getFromDataStoreByCourseNum(req) {
    console.log("getting some from the store by course_num");
    var num = req.params.course_num;
    var query = datastore
        .createQuery('module')
        .filter('course_num', '=', num)
        .order('course_title', {
        descending: true,
    });
    datastore
        .runQuery(query).then(function (results) {
        console.log("thenning: " + results.length);
        var all_modules = results[0];
        console.log('Modules: ');
        all_modules.forEach(function (module) { return console.log(module.course_title); });
    }).catch(function (error) {
        console.log(error);
    });
    return "all was got by course num";
}
exports.getFromDataStoreByCourseNum = getFromDataStoreByCourseNum;
function getFromDataStoreByDeptCourseNum(req) {
    console.log("getting some from the store by dept and num");
    var dept = req.params.course_dept;
    var num = req.params.course_num;
    var query = datastore
        .createQuery('module')
        .filter('course_dept', '=', dept)
        .filter('course_num', '=', num)
        .order('course_title', {
        descending: true,
    });
    datastore
        .runQuery(query).then(function (results) {
        console.log("thenning: " + results.length);
        var all_modules = results[0];
        console.log('Modules: ');
        all_modules.forEach(function (module) { return console.log(module.course_title); });
    }).catch(function (error) {
        console.log(error);
    });
    return "getting some from the store by dept and num";
}
exports.getFromDataStoreByDeptCourseNum = getFromDataStoreByDeptCourseNum;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiRGF0YU1vZGVsLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vc3JjL2RhdGEtbW9kZWwvRGF0YU1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsSUFBTSxTQUFTLEdBQUcsT0FBTyxDQUFDLHlCQUF5QixDQUFDLENBQUM7QUFDckQsSUFBTSxTQUFTLEdBQUcsMkJBQTJCLENBQUM7QUFDOUMsSUFBTSxTQUFTLEdBQUcsSUFBSSxTQUFTLENBQUMsRUFBRSxTQUFTLEVBQUcsU0FBUyxFQUFFLENBQUMsQ0FBQztBQUczRDtJQUNJLE9BQU8sQ0FBQyxHQUFHLENBQUMsNEJBQTRCLENBQUMsQ0FBQztJQUMxQyxJQUFNLEtBQUssR0FBRyxTQUFTO1NBQ2xCLFdBQVcsQ0FBQyxRQUFRLENBQUM7U0FDckIsS0FBSyxDQUFDLGNBQWMsRUFBRTtRQUNuQixVQUFVLEVBQUUsSUFBSTtLQUNuQixDQUFDLENBQUE7SUFFTixTQUFTO1NBQ0osUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLE9BQU87UUFDekIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxZQUFZLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQzNDLElBQU0sV0FBVyxHQUFHLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUMvQixPQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ3pCLFdBQVcsQ0FBQyxPQUFPLENBQUMsVUFBQSxNQUFNLElBQUksT0FBQSxPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsRUFBaEMsQ0FBZ0MsQ0FBQyxDQUFDO0lBQ3BFLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFTLEtBQUs7UUFDbkIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUN2QixDQUFDLENBQUMsQ0FBQTtJQUVOLE9BQU8saUJBQWlCLENBQUM7QUFDN0IsQ0FBQztBQW5CRCxrREFtQkM7QUFDRCxnQ0FBdUMsR0FBUztJQUM1QyxPQUFPLENBQUMsR0FBRyxDQUFDLHFDQUFxQyxDQUFDLENBQUM7SUFDbkQsSUFBSSxJQUFJLEdBQUcsR0FBRyxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUM7SUFDbEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLENBQUM7SUFDN0IsSUFBTSxLQUFLLEdBQUcsU0FBUztTQUNsQixXQUFXLENBQUMsUUFBUSxDQUFDO1NBQ3JCLE1BQU0sQ0FBQyxhQUFhLEVBQUUsR0FBRyxFQUFFLE1BQU0sQ0FBQztTQUNsQyxLQUFLLENBQUMsY0FBYyxFQUFFO1FBQ25CLFVBQVUsRUFBRSxJQUFJO0tBQ25CLENBQUMsQ0FBQTtJQUVOLFNBQVM7U0FDSixRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsT0FBTztRQUN6QixPQUFPLENBQUMsR0FBRyxDQUFDLFlBQVksR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDM0MsSUFBTSxXQUFXLEdBQUcsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQy9CLE9BQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDekIsV0FBVyxDQUFDLE9BQU8sQ0FBQyxVQUFBLE1BQU0sSUFBSSxPQUFBLE9BQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxFQUFoQyxDQUFnQyxDQUFDLENBQUM7SUFDcEUsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQVMsS0FBSztRQUNuQixPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3ZCLENBQUMsQ0FBQyxDQUFBO0lBQ04sT0FBTyxxQkFBcUIsQ0FBQztBQUNqQyxDQUFDO0FBckJELHdEQXFCQztBQUNELHFDQUE0QyxHQUFTO0lBQ2pELE9BQU8sQ0FBQyxHQUFHLENBQUMsMkNBQTJDLENBQUMsQ0FBQztJQUN6RCxJQUFJLEdBQUcsR0FBRyxHQUFHLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQztJQUNoQyxJQUFNLEtBQUssR0FBRyxTQUFTO1NBQ2xCLFdBQVcsQ0FBQyxRQUFRLENBQUM7U0FDckIsTUFBTSxDQUFDLFlBQVksRUFBRSxHQUFHLEVBQUUsR0FBRyxDQUFDO1NBQzlCLEtBQUssQ0FBQyxjQUFjLEVBQUU7UUFDbkIsVUFBVSxFQUFFLElBQUk7S0FDbkIsQ0FBQyxDQUFBO0lBRU4sU0FBUztTQUNKLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxPQUFPO1FBQ3pCLE9BQU8sQ0FBQyxHQUFHLENBQUMsWUFBWSxHQUFHLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUMzQyxJQUFNLFdBQVcsR0FBRyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDL0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUN6QixXQUFXLENBQUMsT0FBTyxDQUFDLFVBQUEsTUFBTSxJQUFJLE9BQUEsT0FBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLEVBQWhDLENBQWdDLENBQUMsQ0FBQztJQUNwRSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsVUFBUyxLQUFLO1FBQ25CLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDdkIsQ0FBQyxDQUFDLENBQUE7SUFDTixPQUFPLDJCQUEyQixDQUFDO0FBQ3ZDLENBQUM7QUFwQkQsa0VBb0JDO0FBQ0QseUNBQWdELEdBQVM7SUFDckQsT0FBTyxDQUFDLEdBQUcsQ0FBQyw2Q0FBNkMsQ0FBQyxDQUFDO0lBQzNELElBQUksSUFBSSxHQUFHLEdBQUcsQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDO0lBQ2xDLElBQUksR0FBRyxHQUFHLEdBQUcsQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDO0lBQ2hDLElBQU0sS0FBSyxHQUFHLFNBQVM7U0FDbEIsV0FBVyxDQUFDLFFBQVEsQ0FBQztTQUNyQixNQUFNLENBQUMsYUFBYSxFQUFFLEdBQUcsRUFBRSxJQUFJLENBQUM7U0FDaEMsTUFBTSxDQUFDLFlBQVksRUFBRSxHQUFHLEVBQUUsR0FBRyxDQUFDO1NBQzlCLEtBQUssQ0FBQyxjQUFjLEVBQUU7UUFDbkIsVUFBVSxFQUFFLElBQUk7S0FDbkIsQ0FBQyxDQUFBO0lBRU4sU0FBUztTQUNKLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxPQUFPO1FBQ3pCLE9BQU8sQ0FBQyxHQUFHLENBQUMsWUFBWSxHQUFHLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUMzQyxJQUFNLFdBQVcsR0FBRyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDL0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUN6QixXQUFXLENBQUMsT0FBTyxDQUFDLFVBQUEsTUFBTSxJQUFJLE9BQUEsT0FBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLEVBQWhDLENBQWdDLENBQUMsQ0FBQztJQUNwRSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsVUFBUyxLQUFLO1FBQ25CLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDdkIsQ0FBQyxDQUFDLENBQUE7SUFDTixPQUFPLDZDQUE2QyxDQUFDO0FBQ3pELENBQUM7QUF0QkQsMEVBc0JDIn0=