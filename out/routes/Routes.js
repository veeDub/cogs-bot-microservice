"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var DataModel_1 = require("../data-model/DataModel");
function getAllModules() {
    DataModel_1.getAllFromDataStore();
}
exports.getAllModules = getAllModules;
function getAllModulesByDept(req) {
    DataModel_1.getFromDataStoreByDept(req);
}
exports.getAllModulesByDept = getAllModulesByDept;
function getAllModulesByNum(req) {
    DataModel_1.getFromDataStoreByCourseNum(req);
}
exports.getAllModulesByNum = getAllModulesByNum;
function getAllModulesByDeptNum(req) {
    DataModel_1.getFromDataStoreByDeptCourseNum(req);
}
exports.getAllModulesByDeptNum = getAllModulesByDeptNum;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiUm91dGVzLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vc3JjL3JvdXRlcy9Sb3V0ZXMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFFQSxxREFBa0o7QUFFbEo7SUFDSSwrQkFBbUIsRUFBRSxDQUFDO0FBQzFCLENBQUM7QUFGRCxzQ0FFQztBQUNELDZCQUFvQyxHQUFTO0lBQ3pDLGtDQUFzQixDQUFDLEdBQUcsQ0FBQyxDQUFDO0FBQ2hDLENBQUM7QUFGRCxrREFFQztBQUNELDRCQUFtQyxHQUFRO0lBQ3ZDLHVDQUEyQixDQUFDLEdBQUcsQ0FBQyxDQUFDO0FBQ3JDLENBQUM7QUFGRCxnREFFQztBQUNELGdDQUF1QyxHQUFRO0lBQzNDLDJDQUErQixDQUFDLEdBQUcsQ0FBQyxDQUFDO0FBQ3pDLENBQUM7QUFGRCx3REFFQyJ9