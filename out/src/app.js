'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
var DataModel_1 = require("./data-model/DataModel");
var finalhandler = require('finalhandler');
var http = require('http');
var Router = require('router');
var router = Router();
router.get('/', function (req, res) {
    return res.end("Endpoints: /modules, /modules/by-dept/:course_dept, /modules/by-faculty/:faculty, /modules/by-year/:year");
});
router.get('/modules', function (req, res) {
    return DataModel_1.getAllFromDataStore().then(function (results) {
        return res.end(JSON.stringify(results));
    });
});
router.get('/modules/by-dept/:course_dept', function (req, res) {
    return DataModel_1.getFromDataStoreByDept(req).then(function (results) {
        return res.end(JSON.stringify(results));
    });
});
router.get('/modules/by-faculty/:faculty', function (req, res) {
    return DataModel_1.getFromDataStoreByFaculty(req).then(function (results) {
        return res.end(JSON.stringify(results));
    });
});
router.get('/modules/by-year/:year', function (req, res) {
    return DataModel_1.getFromDataStoreByCourseNum(req).then(function (results) {
        return res.end(JSON.stringify(results));
    });
});
router.get('/modules/is-module/:course_dept/:course_num', function (req, res) {
    return DataModel_1.getFromDataStoreByDeptCourseNum(req).then(function (results) {
        return res.end(JSON.stringify(results));
    });
});
var app = http.createServer(function (req, res) {
    router(req, res, finalhandler(req, res));
});
app.listen(8080, console.log("Server started at port 8080."));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vc3JjL2FwcC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFHQSxZQUFZLENBQUE7O0FBR1osb0RBQ2tHO0FBRWxHLElBQUksWUFBWSxHQUFHLE9BQU8sQ0FBQyxjQUFjLENBQUMsQ0FBQztBQUMzQyxJQUFJLElBQUksR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7QUFDM0IsSUFBSSxNQUFNLEdBQUcsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDO0FBRy9CLElBQUksTUFBTSxHQUFHLE1BQU0sRUFBRSxDQUFDO0FBRXRCLE1BQU0sQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLFVBQUMsR0FBRyxFQUFFLEdBQUc7SUFDckIsT0FBQSxHQUFHLENBQUMsR0FBRyxDQUFDLDBHQUEwRyxDQUFDO0FBQW5ILENBQW1ILENBQ3RILENBQUE7QUFDRCxNQUFNLENBQUMsR0FBRyxDQUFDLFVBQVUsRUFBRSxVQUFDLEdBQUcsRUFBRSxHQUFHO0lBQzVCLE9BQUEsK0JBQW1CLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBQyxPQUFPO1FBQy9CLE9BQUEsR0FBRyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQWhDLENBQWdDLENBQ25DO0FBRkQsQ0FFQyxDQUNKLENBQUM7QUFDRixNQUFNLENBQUMsR0FBRyxDQUFDLCtCQUErQixFQUFFLFVBQUMsR0FBRyxFQUFFLEdBQUc7SUFDakQsT0FBQSxrQ0FBc0IsQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQyxPQUFPO1FBQ3JDLE9BQUEsR0FBRyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQWhDLENBQWdDLENBQ25DO0FBRkQsQ0FFQyxDQUNKLENBQUM7QUFDRixNQUFNLENBQUMsR0FBRyxDQUFDLDhCQUE4QixFQUFFLFVBQUMsR0FBRyxFQUFFLEdBQUc7SUFDaEQsT0FBQSxxQ0FBeUIsQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQyxPQUFPO1FBQ3hDLE9BQUEsR0FBRyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQWhDLENBQWdDLENBQ25DO0FBRkQsQ0FFQyxDQUNKLENBQUM7QUFDRixNQUFNLENBQUMsR0FBRyxDQUFDLHdCQUF3QixFQUFFLFVBQUMsR0FBRyxFQUFFLEdBQUc7SUFDdEMsT0FBQSx1Q0FBMkIsQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQyxPQUFPO1FBQzFDLE9BQUEsR0FBRyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQWhDLENBQWdDLENBQ25DO0FBRkQsQ0FFQyxDQUNSLENBQUE7QUFFRCxNQUFNLENBQUMsR0FBRyxDQUFDLDZDQUE2QyxFQUFFLFVBQUMsR0FBRyxFQUFFLEdBQUc7SUFDL0QsT0FBQSwyQ0FBK0IsQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQyxPQUFPO1FBQzlDLE9BQUEsR0FBRyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQWhDLENBQWdDLENBQ25DO0FBRkQsQ0FFQyxDQUNKLENBQUE7QUFFRCxJQUFJLEdBQUcsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFVBQVMsR0FBRyxFQUFFLEdBQUc7SUFDekMsTUFBTSxDQUFDLEdBQUcsRUFBRSxHQUFHLEVBQUUsWUFBWSxDQUFDLEdBQUcsRUFBRSxHQUFHLENBQUMsQ0FBQyxDQUFDO0FBQzdDLENBQUMsQ0FBQyxDQUFBO0FBRUYsR0FBRyxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsT0FBTyxDQUFDLEdBQUcsQ0FBQyw4QkFBOEIsQ0FBQyxDQUFDLENBQUMifQ==