"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Datastore = require('@google-cloud/datastore');
var projectID = "microservice-modules-cogs";
var datastore = new Datastore({ projectId: projectID });
var Promise = require("bluebird");
function getAllFromDataStore() {
    return new Promise(function (resolve, reject) {
        var query = datastore
            .createQuery('module')
            .order('course_title', {
            descending: false,
        });
        datastore
            .runQuery(query).then(function (results) {
            return resolve(results[0]);
        }).catch(function (error) {
            return reject(error);
        });
    });
}
exports.getAllFromDataStore = getAllFromDataStore;
function getFromDataStoreByDept(req) {
    return new Promise(function (resolve, reject) {
        var dept = req.params.course_dept;
        var query = datastore
            .createQuery('module')
            .filter('course_dept', '=', dept)
            .order('course_dept', {
            descending: false,
        });
        datastore
            .runQuery(query).then(function (results) {
            return resolve(results[0]);
        }).catch(function (error) {
            return reject(error);
        });
    });
}
exports.getFromDataStoreByDept = getFromDataStoreByDept;
function getFromDataStoreByFaculty(req) {
    return new Promise(function (resolve, reject) {
        var faculty = req.params.faculty;
        var query = datastore
            .createQuery('module')
            .filter('faculty', '=', faculty)
            .order('course_dept', {
            descending: false,
        });
        datastore
            .runQuery(query).then(function (results) {
            return resolve(results[0]);
        }).catch(function (error) {
            return reject(error);
        });
    });
}
exports.getFromDataStoreByFaculty = getFromDataStoreByFaculty;
function getFromDataStoreByCourseNum(req) {
    return new Promise(function (resolve, reject) {
        var num = parseInt(req.params.year);
        var query = datastore
            .createQuery('module')
            .filter('course_num', '>', num)
            .filter('course_num', '<', num + 100);
        datastore
            .runQuery(query).then(function (results) {
            results[0].forEach(function (module) { return console.log(module.course_title); });
            return resolve(results[0]);
        }).catch(function (error) {
            return reject(error);
        });
    });
}
exports.getFromDataStoreByCourseNum = getFromDataStoreByCourseNum;
function getFromDataStoreByDeptCourseNum(req) {
    return new Promise(function (resolve, reject) {
        var dept = req.params.course_dept;
        var num = parseInt(req.params.course_num);
        var query = datastore
            .createQuery('module')
            .filter('course_dept', '=', dept)
            .filter('course_num', '=', num);
        datastore
            .runQuery(query).then(function (results) {
            return resolve(results[0]);
        }).catch(function (error) {
            return reject(error);
        });
    });
}
exports.getFromDataStoreByDeptCourseNum = getFromDataStoreByDeptCourseNum;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiRGF0YU1vZGVsLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL2RhdGEtbW9kZWwvRGF0YU1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsSUFBTSxTQUFTLEdBQUcsT0FBTyxDQUFDLHlCQUF5QixDQUFDLENBQUM7QUFDckQsSUFBTSxTQUFTLEdBQUcsMkJBQTJCLENBQUM7QUFDOUMsSUFBTSxTQUFTLEdBQUcsSUFBSSxTQUFTLENBQUMsRUFBRSxTQUFTLEVBQUcsU0FBUyxFQUFFLENBQUMsQ0FBQztBQUMzRCxrQ0FBb0M7QUFHcEM7SUFDSSxPQUFPLElBQUksT0FBTyxDQUFNLFVBQUMsT0FBTyxFQUFFLE1BQU07UUFDcEMsSUFBTSxLQUFLLEdBQUcsU0FBUzthQUNsQixXQUFXLENBQUMsUUFBUSxDQUFDO2FBQ3JCLEtBQUssQ0FBQyxjQUFjLEVBQUU7WUFDbkIsVUFBVSxFQUFFLEtBQUs7U0FDcEIsQ0FBQyxDQUFDO1FBRVAsU0FBUzthQUNKLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxPQUFPO1lBQzdCLE9BQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQy9CLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFTLEtBQUs7WUFDbkIsT0FBTyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDekIsQ0FBQyxDQUFDLENBQUE7SUFDTixDQUFDLENBQUMsQ0FBQTtBQUNOLENBQUM7QUFmRCxrREFlQztBQUNELGdDQUF1QyxHQUFTO0lBQzVDLE9BQU8sSUFBSSxPQUFPLENBQU0sVUFBQyxPQUFPLEVBQUUsTUFBTTtRQUNwQyxJQUFJLElBQUksR0FBRyxHQUFHLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQztRQUNsQyxJQUFNLEtBQUssR0FBRyxTQUFTO2FBQ2xCLFdBQVcsQ0FBQyxRQUFRLENBQUM7YUFDckIsTUFBTSxDQUFDLGFBQWEsRUFBRSxHQUFHLEVBQUUsSUFBSSxDQUFDO2FBQ2hDLEtBQUssQ0FBQyxhQUFhLEVBQUU7WUFDbEIsVUFBVSxFQUFFLEtBQUs7U0FDcEIsQ0FBQyxDQUFBO1FBRU4sU0FBUzthQUNKLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxPQUFPO1lBQ3pCLE9BQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ25DLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFTLEtBQUs7WUFDbkIsT0FBTyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDekIsQ0FBQyxDQUFDLENBQUE7SUFDTixDQUFDLENBQUMsQ0FBQTtBQUNOLENBQUM7QUFqQkQsd0RBaUJDO0FBRUQsbUNBQTBDLEdBQVM7SUFDL0MsT0FBTyxJQUFJLE9BQU8sQ0FBTSxVQUFDLE9BQU8sRUFBRSxNQUFNO1FBQ3BDLElBQUksT0FBTyxHQUFHLEdBQUcsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDO1FBQ2pDLElBQU0sS0FBSyxHQUFHLFNBQVM7YUFDbEIsV0FBVyxDQUFDLFFBQVEsQ0FBQzthQUNyQixNQUFNLENBQUMsU0FBUyxFQUFFLEdBQUcsRUFBRSxPQUFPLENBQUM7YUFDL0IsS0FBSyxDQUFDLGFBQWEsRUFBRTtZQUNsQixVQUFVLEVBQUUsS0FBSztTQUNwQixDQUFDLENBQUE7UUFFTixTQUFTO2FBQ0osUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLE9BQU87WUFDN0IsT0FBTyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDL0IsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQVMsS0FBSztZQUNuQixPQUFPLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUN6QixDQUFDLENBQUMsQ0FBQTtJQUNOLENBQUMsQ0FBQyxDQUFBO0FBQ04sQ0FBQztBQWpCRCw4REFpQkM7QUFDRCxxQ0FBNEMsR0FBUztJQUNqRCxPQUFPLElBQUksT0FBTyxDQUFNLFVBQUMsT0FBTyxFQUFFLE1BQU07UUFDcEMsSUFBSSxHQUFHLEdBQVcsUUFBUSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDNUMsSUFBTSxLQUFLLEdBQUcsU0FBUzthQUNsQixXQUFXLENBQUMsUUFBUSxDQUFDO2FBQ3JCLE1BQU0sQ0FBQyxZQUFZLEVBQUUsR0FBRyxFQUFFLEdBQUcsQ0FBQzthQUM5QixNQUFNLENBQUMsWUFBWSxFQUFFLEdBQUcsRUFBRSxHQUFHLEdBQUcsR0FBRyxDQUFDLENBQUE7UUFDekMsU0FBUzthQUNKLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxPQUFPO1lBQ3pCLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsVUFBQSxNQUFNLElBQUksT0FBQSxPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsRUFBaEMsQ0FBZ0MsQ0FBQyxDQUFDO1lBQy9ELE9BQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ25DLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFTLEtBQUs7WUFDbkIsT0FBTyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDekIsQ0FBQyxDQUFDLENBQUE7SUFDTixDQUFDLENBQUMsQ0FBQTtBQUNOLENBQUM7QUFmRCxrRUFlQztBQUNELHlDQUFnRCxHQUFTO0lBQ3JELE9BQU8sSUFBSSxPQUFPLENBQU0sVUFBQyxPQUFPLEVBQUUsTUFBTTtRQUNwQyxJQUFJLElBQUksR0FBRyxHQUFHLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQztRQUNsQyxJQUFJLEdBQUcsR0FBVyxRQUFRLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUNsRCxJQUFNLEtBQUssR0FBRyxTQUFTO2FBQ2xCLFdBQVcsQ0FBQyxRQUFRLENBQUM7YUFDckIsTUFBTSxDQUFDLGFBQWEsRUFBRSxHQUFHLEVBQUUsSUFBSSxDQUFDO2FBQ2hDLE1BQU0sQ0FBQyxZQUFZLEVBQUUsR0FBRyxFQUFFLEdBQUcsQ0FBQyxDQUFBO1FBRW5DLFNBQVM7YUFDSixRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsT0FBTztZQUN6QixPQUFPLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUMvQixDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsVUFBUyxLQUFLO1lBQ25CLE9BQU8sTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzdCLENBQUMsQ0FBQyxDQUFBO0lBQ04sQ0FBQyxDQUFDLENBQUE7QUFDTixDQUFDO0FBaEJELDBFQWdCQyJ9