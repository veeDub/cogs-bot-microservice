/**
 * Created by valeriewyns on 2018-05-06.
 */
'use strict'

import bodyParser = require("body-parser");
import {getAllFromDataStore, getFromDataStoreByDept, getFromDataStoreByCourseNum,
        getFromDataStoreByDeptCourseNum, getFromDataStoreByFaculty} from "./data-model/DataModel";

let finalhandler = require('finalhandler');
let http = require('http');
let Router = require('router');


let router = Router();

router.get('/', (req, res) =>
    res.end("Endpoints: /modules, /modules/by-dept/:course_dept, /modules/by-faculty/:faculty, /modules/by-year/:year")
)
router.get('/modules', (req, res) =>
    getAllFromDataStore().then((results) =>
        res.end(JSON.stringify(results))
    )
);
router.get('/modules/by-dept/:course_dept', (req, res) =>
    getFromDataStoreByDept(req).then((results) =>
        res.end(JSON.stringify(results))
    )
);
router.get('/modules/by-faculty/:faculty', (req, res) =>
    getFromDataStoreByFaculty(req).then((results) =>
        res.end(JSON.stringify(results))
    )
);
router.get('/modules/by-year/:year', (req, res) =>
        getFromDataStoreByCourseNum(req).then((results) =>
            res.end(JSON.stringify(results))
        )
)

router.get('/modules/is-module/:course_dept/:course_num', (req, res) =>
    getFromDataStoreByDeptCourseNum(req).then((results) =>
        res.end(JSON.stringify(results))
    )
)

let app = http.createServer(function(req, res) {
    router(req, res, finalhandler(req, res));
})

app.listen(8080, console.log("Server started at port 8080."));
