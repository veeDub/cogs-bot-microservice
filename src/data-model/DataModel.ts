const Datastore = require('@google-cloud/datastore');
const projectID = "microservice-modules-cogs";
const datastore = new Datastore({ projectId : projectID });
import * as Promise from 'bluebird';


export function getAllFromDataStore() : Promise<any> {
    return new Promise<any>((resolve, reject) => {
        const query = datastore
            .createQuery('module')
            .order('course_title', {
                descending: false,
            });

        datastore
            .runQuery(query).then(results => {
            return resolve(results[0]);
        }).catch(function(error){
            return reject(error);
        })
    })
}
export function getFromDataStoreByDept(req : any) : Promise<any>{
    return new Promise<any>((resolve, reject) => {
        let dept = req.params.course_dept;
        const query = datastore
            .createQuery('module')
            .filter('course_dept', '=', dept)
            .order('course_dept', {
                descending: false,
            })

        datastore
            .runQuery(query).then(results => {
                return resolve(results[0]);
        }).catch(function(error){
            return reject(error);
        })
    })
}

export function getFromDataStoreByFaculty(req : any) : Promise<any>{
    return new Promise<any>((resolve, reject) => {
        let faculty = req.params.faculty;
        const query = datastore
            .createQuery('module')
            .filter('faculty', '=', faculty)
            .order('course_dept', {
                descending: false,
            })

        datastore
            .runQuery(query).then(results => {
            return resolve(results[0]);
        }).catch(function(error){
            return reject(error);
        })
    })
}
export function getFromDataStoreByCourseNum(req : any) : Promise<any>  {
    return new Promise<any>((resolve, reject) => {
        let num: number = parseInt(req.params.year);
        const query = datastore
            .createQuery('module')
            .filter('course_num', '>', num)
            .filter('course_num', '<', num + 100)
        datastore
            .runQuery(query).then(results => {
                results[0].forEach(module => console.log(module.course_title));
                return resolve(results[0]);
        }).catch(function(error){
            return reject(error);
        })
    })
}
export function getFromDataStoreByDeptCourseNum(req : any) : Promise<any> {
    return new Promise<any>((resolve, reject) => {
        let dept = req.params.course_dept;
        let num: number = parseInt(req.params.course_num);
        const query = datastore
            .createQuery('module')
            .filter('course_dept', '=', dept)
            .filter('course_num', '=', num)

        datastore
            .runQuery(query).then(results => {
                return resolve(results[0]);
            }).catch(function(error){
                return reject(error);
        })
    })
}





